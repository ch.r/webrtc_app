// import {computed, observable} from "mobx";
import apiClient, { USER_TOKEN_KEY } from "../api/api-client";
import { parseJwt } from "../api/jwt";
import { UserOnboarding, Extension } from "../model/user-onboarding";
import { UserSignUp } from "../model/authentication";
import { handleFormSubmit } from "../lib/final-form/final-form";



export type JwtUser = {
  userId: string;
};

class AuthStore {

  userSignUp?: UserSignUp;
  userOnboarding?: UserOnboarding;
  extensions?: Extension[]|null;
  extension?: Extension;
  isAppLoaded = false;

  async fetchAuthUser(token?: string) {
    const jwt = token || localStorage.getItem(USER_TOKEN_KEY);
    if (!jwt) {
      this.isAppLoaded = true;
      return;
    }

    let jwtUser: JwtUser | undefined;
    try {
      jwtUser = parseJwt<JwtUser>(jwt);
      localStorage.setItem(USER_TOKEN_KEY, jwt);
      sessionStorage.setItem("login_time", "" + new Date().getTime());
    } catch (e) {
      console.error(e);
      this.isAppLoaded = true;
      return;
    }
    const user: UserOnboarding = {
      userid: jwtUser.userId,
    };
    this.userOnboarding = user;
    this.extensions=await apiClient.userOnboardingFetch();
    console.log("kk",this.extension)
    this.extensions?.forEach((ex) => {
      if (ex.owner === this.userOnboarding?.userid) {
        this.extension = ex;
      }
    });
    this.isAppLoaded = true;
  }
  async  userOnboardingFetch(){

   const response=await handleFormSubmit(apiClient.userOnboardingFetch());
    return response;

  }

}

// decorate(AuthStore, {
//     isAppLoaded: observable,
//     isAuthenticated: computed,
// });

export default new AuthStore();
