import apiClient from '../api/api-client'
import authStore from './auth-store';
import {handleFormSubmit} from "../lib/final-form/final-form";


export type LoginForm = {
    username: string;
    password: string;
    formScenario?: 'sign_in' | 'reset_password';
}

class LoginStore {
    loginForm?: LoginForm;
  
    submitLoginForm = async (form: LoginForm) => {
        const response = await handleFormSubmit((apiClient.login(form.username, form.password)));
        if (response.err) {
            return response.err["FINAL_FORM/form-error"]
        }
    
        const token  = response.response;
        await authStore.fetchAuthUser(token).then();
    }

}

// decorate(LoginStore, {
//     loginForm: observable,
//     submitLoginForm: action
// })

export default new LoginStore();