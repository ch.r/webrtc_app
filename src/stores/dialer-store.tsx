import apiClient from "../api/api-client";
import { handleFormSubmit } from "../lib/final-form/final-form";
import { Extension } from "../model/user-onboarding";

export type Call = {
  ptransferred: string;
  scustomer: string;
  recording: string;
  stype: string;
  stransferred: string;
  channel: string;
  callerid_external: string;
  callerid_internal: string;
  dtype: string;
  dnumber: string;
  spresent: string;
  server: string;
  caller: string;
  callername_internal: string;
  callid: string;
  machine: string;
  called: string;
  ctype: string;
  bridged_callid: string;
  uniqueid: string;
  note: string;
  outgroup: string;
  ingroup: string;
  callername_external: string;
  dtransferred: string;
  peer: string;
  cnumber: string;
  customer_name: string;
  answered: string;
  snumber: string;
  start: string;
};

class DialerStore {

  getActivecalls = async () => {

    const response = await handleFormSubmit(apiClient.getActiveCalls()) ;
    return response;

  };

  isActive = async (activeCalls: Call[], ex: Extension) => {
    if (activeCalls)
      for (var i = 0; i < activeCalls.length; i++) {
        if (
          activeCalls[i].called === ex.name ||
          activeCalls[i].caller === ex.name
        ) {
          return true;
        }
      }
    return false;
  };
}

export default new DialerStore();
