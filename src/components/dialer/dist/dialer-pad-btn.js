"use strict";
exports.__esModule = true;
exports.DialerPadBtn = void 0;
exports.DialerPadBtn = function (props) {
    var touch = props.touch, number = props.number, onClick = props.onClick, order = props.order;
    return (React.createElement("button", { className: touch ? "numberBtn numberBtnTouch" : "numberBtn", onClick: onClick, style: { order: order } }, number));
};
exports["default"] = exports.DialerPadBtn;
