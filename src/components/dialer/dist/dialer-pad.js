"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.DialerPad = void 0;
var react_1 = require("react");
var dialer_pad_btn_1 = require("./dialer-pad-btn");
var sip_1 = require("../../sip/sip");
exports.DialerPad = function (props) {
    var _a = react_1.useState(""), numberToCall = _a[0], setNumberToCall = _a[1];
    var _b = react_1.useState(), touch = _b[0], setTouch = _b[1];
    var numbers = {
        1: { letters: "END", order: 1 },
        2: { letters: "ABC", order: 1 },
        3: { letters: "DEF", order: 1 },
        4: { letters: "GHI", order: 2 },
        5: { letters: "JKL", order: 2 },
        6: { letters: "MNO", order: 2 },
        7: { letters: "PQRS", order: 3 },
        8: { letters: "TUV", order: 3 },
        9: { letters: "WXYZ", order: 3 },
        "*": { letters: "*", order: 4 },
        0: { letters: "+", order: 4 },
        "#": { letters: "#", order: 4 }
    };
    var playSound = (function beep() {
        var snd = new Audio("data:audio/wav;base64,SUQzAwAAAAADHlRBTEIAAAApAAAB//5mAHIAZQBlAHMAbwB1AG4AZABlAGYAZgBlAGMAdAAuAG4AZQB0AFRQRTEAAAApAAAB//5mAHIAZQBlAHMAbwB1AG4AZABlAGYAZgBlAGMAdAAuAG4AZQB0AFRZRVIAAAALAAAB//4yADAAMQA2AFREQVQAAAALAAAB//4wADEAMAAxAFRFTkMAAABTAAAB//5XAGEAdgBlAFAAYQBkACAARgByAGUAZQAgAEgAbwBtAGUAIABWAGUAcgBzAGkAbwBuACAAqQAgAE4AQwBIACAAUwBvAGYAdAB3AGEAcgBlAFRDT04AAAALAAAB//4oADEANwApAFRJVDIAAABTAAAB//5jAGwAaQBjAGsAIABmAGkAbgBnAGUAcgBzACAAIABzAGkAbgBnAGwAZQAgAGMAbABpAGMAawAgAHMAbwB1AG4AZAAgAGUAZgBmAGUAYwB0AFRSQ0sAAAANAAAB//4wADQALwAxADYAQ09NTQAAAB4AAAFlbmf//gAA//5lAHgAYwBlAGwAbABlAG4AdAAhAP/zgAQAAAAAAAAAAAAAAAAAAAAAAEluZm8AAAAPAAAAAwAAA0IAVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq////////////////////////////////////////////AAAAOUxBTUUzLjk5cgFuAAAAACxoAAAUQCQEaSYAAEAAAANC88aJlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/84AEAAgAA2aqBAAAEQAG0jYIRgAgcIAXBnyYIAg6fKAgc5QEATP5QHwfB8P8Tg+CBzKAgCByo5+gHw/y4Pg+f6wcBAEPEAIAN///pE4f//lAwn+XkajpVVgAoOoEAQD5fBMHwfP8uD5rpBAEAQ4gBAEAfflwfB8H/E4E8oCDv/+sHwfD/pBAEAQOfB8Hz/7v1AgD4f/+UBAHSBBAqCoKnVgqCoKgsHcFQaBoO1A0DQNA0dqBoGQVBUFdYKgqC2VBUGj2LA0DQNcGgaBkNZYF//OCBDcKbANktQAjABUIAtZcAEYAQVBUNawVBUGgafqBoGgaJf///iUFQafxEDQNA1J63WkiUojBYGgaDosDQNA0DQK1A0DIKncSgqCoKndYKg0DXEQNA0DR3EQMgqCrsSgqCp0O4lEXBY95YGgaPdQlBUFTvrEQNB0O+DQKgqCv1g0DQdVMQU1FMy45OUxBTUUzLjk5LjVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVX/84IEQwAAAaQAAAAAAAADSAAAAABVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVRBRwBsaWNrIGZpbmdlcnMgIHNpbmdsZSBjbGljayBzb2ZyZWVzb3VuZGVmZmVjdC5uZXQAAAAAAAAAAAAAAGZyZWVzb3VuZGVmZmVjdC5uZXQAAAAAAAAAAAAAADIwMTYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQR");
        return function () {
            snd.play();
        };
    })();
    var onKeyDown = function (eve) {
        if ((eve.key >= "0" && eve.key <= "9" && !eve.shiftKey) ||
            (Number(eve.key) >= 96 && Number(eve.key) <= 107) ||
            eve.key === "#" ||
            eve.key === "*") {
            digitBtn(eve.key);
        }
        else if (eve.key === "Backspace") {
            onClickDelete();
        }
        else if (eve.key === "Eanter")
            calling();
        eve.preventDefault();
    };
    var digitBtn = function (num) {
        setTouch(num);
        playSound();
        if (numberToCall.length < 16) {
            setNumberToCall(numberToCall + num);
        }
    };
    var onClickDelete = function () {
        playSound();
        setNumberToCall(numberToCall.slice(0, -1));
    };
    var getSipNumber = function () {
        if (numberToCall.length < 7) {
            return "sip:-" + numberToCall + "@" + sip_1.domain;
        }
        return "sip:" + numberToCall + "@" + sip_1.domain;
    };
    function calling() {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                props.history.push("dialer/calling", { destination: getSipNumber() });
                return [2 /*return*/];
            });
        });
    }
    react_1.useEffect(function () {
        var _a;
        (_a = document.getElementById("dialer")) === null || _a === void 0 ? void 0 : _a.focus();
    });
    return (React.createElement("div", { className: "dialer", id: "dialer", onKeyDown: onKeyDown, tabIndex: 0 },
        React.createElement("div", { className: "demo-window" }),
        React.createElement("audio", { id: "remoteAudio" }),
        React.createElement("div", { className: "clearfix" }),
        numberToCall === "" ? (React.createElement("div", { className: "msg" }, "Type a number or extension")) : (React.createElement("div", null,
            React.createElement("div", { className: "number" }, numberToCall),
            React.createElement("button", { className: "delete", onClick: onClickDelete }, "x"))),
        React.createElement("div", { className: "digits" },
            React.createElement("div", { className: "" }, Object.entries(numbers).map(function (_a) {
                var num = _a[0], val = _a[1];
                return (React.createElement(dialer_pad_btn_1["default"], { touch: touch === num ? true : false, number: num, onClick: function () { return digitBtn(num); }, order: val.order, key: num }));
            }))),
        React.createElement("button", { className: "background-green btn call", onClick: calling },
            React.createElement("img", { className: "div-center", src: "https://webrtc.phone.do/images/dial.png", alt: "dial" }))));
};
exports["default"] = exports.DialerPad;
