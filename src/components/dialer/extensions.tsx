import { useEffect, useState } from "react";
// import { setSourceMapRange } from "typescript";
import { Extension } from "../../model/user-onboarding";
import dialerStore, { Call } from "../../stores/dialer-store";
import { History } from "history";
import ExtensionBtn from "./extension-btn";
import authStore from "../../stores/auth-store";

type Props = {
  history: History<{}>;
};

export const Extensions = (props: Props) => {
  const [activeCalls, setActiveCalls] = useState<Call[] | null>([]);
  const [extensions, setExtensions] = useState<Extension[] | null>([]);
  const [errors, setErrors] = useState<string>("");

  async function userOnboardingFetch() {
    authStore
      .userOnboardingFetch()
      .then((response) => {
        if (response.err) setErrors(response.err["FINAL_FORM/form-error"]);
        else setExtensions(response.response);
      })
      .catch((err) => {
        console.log("err1", err);
      });
  }

  async function getActivecalls() {
    const response = await dialerStore.getActivecalls();
    if (response.err) setErrors(response.err["FINAL_FORM/form-error"]);
    else setActiveCalls(response.response);
  }

  useEffect(() => {
    if (authStore.extensions) setExtensions(authStore.extensions);
    else userOnboardingFetch();
    getActivecalls();
  }, []);

  return (
    <>
      <div className="extensions btn-group">
        <div className="extension-title">All extensions</div>
        {extensions
          ? extensions.map((ex) => (
              <ExtensionBtn
                history={props.history}
                activeCalls={activeCalls}
                ex={ex}
              ></ExtensionBtn>
            ))
          : null}
      </div>
      <div className="err">{errors}</div>
    </>
  );
};

export default Extensions;
