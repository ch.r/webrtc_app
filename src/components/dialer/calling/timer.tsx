import { observer } from "mobx-react";
import Timer from "react-compound-timer";
type Props = {
  format: string
};
export const MyTimer =observer((props: Props) => {
  return (
    <Timer
      formatValue={(value: number) => `${value < 10 ? `0${value}` : value}`}
    >
      {" "}
      <Timer.Days
        formatValue={(value: number) =>
          `${value > 0 ? (value < 10 ? `0${value}:` : `${value}:`) : ""}`
        }
      />{" "}
      <Timer.Hours
        formatValue={(value: number) =>
          `${value > 0 ? (value < 10 ? `0${value}:` : `${value}:`) : ""}`
        }
      />
      <Timer.Minutes />:<Timer.Seconds />{" "}
    </Timer>
 );
});
export default MyTimer;
