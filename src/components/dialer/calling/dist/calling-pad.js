"use strict";
exports.__esModule = true;
exports.CallingPad = void 0;
var timer_1 = require("./timer");
var date_1 = require("../date");
var sip_1 = require("../../../sip/sip");
var react_1 = require("react");
var mute_white_png_1 = require("../../../assets/icons/mute_white.png");
var un_mute_png_1 = require("../../../assets/icons/un_mute.png");
var hold_png_1 = require("../../../assets/icons/hold.png");
var un_hold_png_1 = require("../../../assets/icons/un_hold.png");
exports.CallingPad = function (props) {
    var _a = react_1.useState(false), isMute = _a[0], setIsMute = _a[1];
    var _b = react_1.useState(false), isHold = _b[0], setIsHold = _b[1];
    function hold() {
        if (isHold) {
            sip_1.simpleUser.unhold()["catch"](function (err) {
                console.log(err);
            });
            setIsHold(!isHold);
        }
        else {
            setIsHold(!isHold);
            sip_1.simpleUser.hold()["catch"](function (err) {
                console.log(err);
            });
        }
    }
    function mute() {
        if (isMute) {
            try {
                sip_1.simpleUser.unmute();
                setIsMute(!isMute);
            }
            catch (err) {
                console.log(err);
            }
        }
        else {
            try {
                setIsMute(!isMute);
                sip_1.simpleUser.mute();
            }
            catch (err) {
                console.log(err);
            }
        }
    }
    return (React.createElement("div", { className: "calling-pad" },
        React.createElement("div", null,
            React.createElement("h2", { className: "title calling title-top" }, "Incomming Call"),
            React.createElement("h2", { className: "title calling-number title-top" },
                "(",
                props.number ? props.number.slice(0, props.number.length - 9) : null,
                ") ",
                props.number ? props.number.slice(-9) : null)),
        React.createElement("div", null,
            React.createElement("h4", { className: "title title-center title-left" }, "Duration"),
            React.createElement("h4", { className: "title title-center title-right" },
                React.createElement(timer_1["default"], { format: "number" }))),
        React.createElement("div", { className: "call-menu-calling btn-group" },
            React.createElement("div", { className: "div-menu-calling" },
                React.createElement("button", { className: "btn btn-circle border div-center" },
                    React.createElement("img", { src: "https://webrtc.phone.do/images/Call_Transfer_Icon_White.png", className: "div-center", alt: "call transfer" })),
                React.createElement("p", { className: "title title-small" }, "Call Transfer")),
            React.createElement("div", { className: "div-menu-calling" },
                React.createElement("button", { className: "btn btn-circle border div-center", onClick: mute },
                    React.createElement("img", { src: isMute ? un_mute_png_1["default"] : mute_white_png_1["default"], className: "div-center", alt: "mute" })),
                React.createElement("p", { className: "title title-small" }, isMute ? "Un mute" : "Mute")),
            React.createElement("div", { className: "div-menu-calling" },
                React.createElement("button", { className: "btn btn-circle border div-center", onClick: hold },
                    React.createElement("img", { src: isHold ? un_hold_png_1["default"] : hold_png_1["default"], className: "div-center", alt: "hold" })),
                React.createElement("p", { className: "title title-small" }, isHold ? "Un hold" : "Hold")),
            React.createElement("div", { className: "div-menu-calling" },
                React.createElement("button", { className: "btn btn-circle border div-center" },
                    React.createElement("img", { src: "https://webrtc.phone.do/images/Add_Call.png", className: "div-center", alt: "add call" })),
                React.createElement("p", { className: "title title-small" }, "Add Call")),
            React.createElement("div", { className: "div-menu-calling" },
                React.createElement("button", { className: "btn btn-circle border div-center" },
                    React.createElement("img", { src: "https://webrtc.phone.do/images/Block.png", className: "div-center", alt: "block" })),
                React.createElement("p", { className: "title title-small" }, "Block Call")),
            React.createElement("div", { className: "div-menu-calling" },
                React.createElement("button", { className: "btn btn-circle border div-center" },
                    React.createElement("img", { src: "https://webrtc.phone.do/images/Keyboard.png", className: "div-center", alt: "keyboard" })),
                React.createElement("p", { className: "title title-small" }, "Keyboard"))),
        React.createElement("div", { className: "hangup" },
            React.createElement("button", { className: "background-red btn btn-square", onClick: props.hangupCall },
                React.createElement("img", { className: "div-center", src: "https://webrtc.phone.do/images/dial.png", alt: "dial" }))),
        React.createElement("div", { className: "date-calling" },
            React.createElement(date_1["default"], null))));
};
exports["default"] = exports.CallingPad;
