"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.Calling = void 0;
var mobx_react_1 = require("mobx-react");
var Sidebar_1 = require("../../sidebar/Sidebar");
var sip_1 = require("../../../sip/sip");
require("../../components.css");
require("./calling.css");
var react_1 = require("react");
var animation_calling_png_1 = require("../../../assets/images/animation_calling.png");
var header_1 = require("../../header/header");
var extensions_1 = require("../extensions");
var calling_pad_1 = require("./calling-pad");
var timer_1 = require("./timer");
exports.Calling = mobx_react_1.observer(function (props) {
    var options2 = {
        requestOptions: {
            body: {
                contentDisposition: "render",
                contentType: "application/dtmf-relay",
                content: "Signal=1\r\nDuration=1000"
            }
        }
    };
    var _a = react_1.useState(false), isload = _a[0], setIsload = _a[1];
    var _b = react_1.useState(false), isHangup = _b[0], setIsHangup = _b[1];
    var _c = react_1.useState(false), isTimer = _c[0], setIsTimer = _c[1];
    var _d = react_1.useState("Calling..."), desciptionCall = _d[0], setDesciptionCall = _d[1];
    function calling(destination) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, sip_1.call(destination)["catch"](function (err) {
                            console.log(err);
                        })];
                    case 1:
                        _a.sent();
                        setIsTimer(true);
                        return [2 /*return*/];
                }
            });
        });
    }
    if (!isload) {
        try {
            calling(props.location.state.destination)["catch"](function (err) {
                console.log(err);
            });
        }
        catch (err) {
            console.log(err);
        }
        setIsload(true);
    }
    function hangupCall() {
        return __awaiter(this, void 0, void 0, function () {
            var sec, interval;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        setIsTimer(false);
                        setDesciptionCall("Call ended");
                        setIsHangup(true);
                        return [4 /*yield*/, sip_1.hangup()["catch"](function (err) {
                                console.log(err);
                            })];
                    case 1:
                        _a.sent();
                        sec = 0;
                        interval = setInterval(function () {
                            sec++;
                            if (sec === 2) {
                                clearInterval(interval);
                                props.history.push("/dialer");
                            }
                        }, 1000);
                        return [2 /*return*/];
                }
            });
        });
    }
    function keypad() {
        return __awaiter(this, void 0, void 0, function () {
            var session;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, sip_1.initSession()];
                    case 1:
                        session = _a.sent();
                        session.info({
                            requestOptions: {
                                body: {
                                    contentDisposition: "render",
                                    contentType: "application/dtmf-relay",
                                    content: "Signal=1\r\nDuration=1000"
                                }
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    }
    window.onload = function (e) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        e.preventDefault();
                        return [4 /*yield*/, sip_1.hangup()["catch"](function (err) {
                                console.log(err);
                            })];
                    case 1:
                        _a.sent();
                        props.history.push("/dialer");
                        return [2 /*return*/];
                }
            });
        });
    };
    window.onbeforeunload = function (e) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        e.preventDefault();
                        return [4 /*yield*/, sip_1.hangup()["catch"](function (err) {
                                console.log(err);
                            })];
                    case 1:
                        _a.sent();
                        props.history.push("/dialer");
                        return [2 /*return*/];
                }
            });
        });
    };
    return (React.createElement("div", null,
        React.createElement(header_1["default"], null),
        React.createElement(Sidebar_1.Sidebar, { selected: "dialer", history: props.history }),
        React.createElement(extensions_1["default"], { history: props.history }),
        console.log("destination", props.location.state),
        React.createElement("button", { style: {
                marginLeft: "72vw",
                marginTop: '10vh'
            }, onClick: keypad }, "keypad"),
        !isHangup ? React.createElement(calling_pad_1["default"], { number: props.location.state ? props.location.state.destination.slice(4).split('@')[0] : null, history: props.history, hangupCall: hangupCall }) : null,
        React.createElement("div", { className: "main" },
            React.createElement("audio", { id: "remoteAudio" }),
            React.createElement("p", { className: "desciption-call" }, isTimer ? React.createElement(timer_1["default"], { format: "" }) : desciptionCall),
            React.createElement("img", { className: "animation-calling", src: animation_calling_png_1["default"], alt: "animation-calling" }))));
});
exports["default"] = exports.Calling;
