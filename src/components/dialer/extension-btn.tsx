import { useEffect, useState } from "react";
// import { setSourceMapRange } from "typescript";
import { Extension } from "../../model/user-onboarding";
import dialerStore, { Call } from "../../stores/dialer-store";
import red_headphones from "../../assets/icons/red_headphones.png"
import { domain } from "../../sip/sip"
import { History } from "history";

type Props = {
  history: History<{}>;
  ex: Extension;
  activeCalls: Call[]|null;
};

export const ExtensionBtn = (props: Props) => {
  const { ex, activeCalls } = props;
  let [src, setSrc] = useState("");

  useEffect(() => {
    if(activeCalls)
    dialerStore.isActive(activeCalls, ex).then((response) => {
      if (response) {
        setSrc("https://webrtc.phone.do/images/green_ellipse.png");
      } else {
        if (!ex.contact) {
          setSrc("https://webrtc.phone.do/images/red_ellipse.png");
        } else {
          setSrc("https://webrtc.phone.do/images/orange_ellipse.png");
        }
      }
    });
  }, [activeCalls, ex]);

  async function callExtension(extension:string) { 

   props.history.push("dialer/calling",{destination:"sip:"+extension+"@"+domain});
   
  }

  return (
    <button key={ex.name} className="dialer-btn extension-btn">
      <button onClick={()=>callExtension(ex.callerid_external)} className="headphones">
      <img
        src={red_headphones}
        className="btn-icon call-menu-icon-left"
        alt=""
      />
      </button>
      <img
        src={src}
        className="btn-icon call-menu-icon-right"
        alt="status"
      />
      {ex.description ? ex.description : ex.name}
    </button>
  );
};

export default ExtensionBtn;
