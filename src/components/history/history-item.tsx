import { HistoryCall } from "../../stores/history-store";

type Props = {
  call: HistoryCall;
};

export const HistoryItem = (props: Props) => {
  const { call } = props;

  const format=(num:number)=>{
      return num<10?`0${num}`:`${num}`;
  }

  const hour=Number(call.talktime)>3600?`${format(Math.floor(Number(call.talktime)/3600))}:`:"";
  const minutes=format(Math.floor((Number(call.talktime)%3600)/60));
  const seconds=format(Number(call.talktime)%60);

  const talktime=`${hour}${minutes}:${seconds}`;

  return (
    <div>
      {call.dnumber === localStorage.getItem("extension")
        ? `${call.callerid_internal} <- ${talktime}`
        : `${call.dnumber} ->  ${talktime}`}
    </div>
  );
};
