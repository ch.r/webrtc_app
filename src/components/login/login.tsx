import { useState } from "react";
import { observer } from "mobx-react";
import { Field, Form } from "react-final-form";
import { History } from "history";
import loadingGif from "../../assets/icons/loading.gif";
import loginStore, { LoginForm } from "../../stores/login-store";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import PersonOutlineOutlinedIcon from "@material-ui/icons/PersonOutlineOutlined";
import loginGif from "../../assets/images/login.gif";
import logo from "../../assets/icons/login_logo.png";
import "./login.css";

type Location = {
  hash?: string;
  key?: string;
  pathname?: string;
  search?: string;
  state: { to: string };
};
type Props = {
  history: History<{}>;
  location: Location;
};

export const Login = observer((props: Props) => {
  const { history, location } = props;
  const [errorMessageflag, setErrorMessageflag] = useState(false);
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [loading, setLoading] = useState(false);
  console.log(location, "location");
  return (
    <div className="">
      <link
        href="https://fonts.googleapis.com/css?family=Rubik"
        rel="stylesheet"
      />
      <Form<LoginForm>
        initialValues={loginStore.loginForm}
        onSubmit={async (form) => {
          setLoading(true);
          if (username && password) {
            if (form.formScenario === "sign_in") {
              let submitLoginRes = await loginStore.submitLoginForm({
                username: username,
                password: password,
              });
              if (submitLoginRes) {
                setLoading(false);
                setErrorMessageflag(submitLoginRes);
                return submitLoginRes;
              }
              return history.push(
                location.state ? location.state.to : "/dialer"
              );
            } else
              throw new Error("Invalid form scenario: " + form.formScenario);
          }
        }}
      >
        {({ submitError, handleSubmit, submitting, pristine, form }) => (
          <form onSubmit={handleSubmit} className="right-side">
            <img src={loginGif} alt="login gif" className="right-side-img" />
            <div className="login-form">
              <img src={logo} alt="phondo" className="logo"></img>
              <div>
                <h1>Welcome to Phone.do</h1>
                <h1>Login to getting started</h1>
                <h3>enter your details to proceed further</h3>
              </div>
              <div className="field">
                <label>Username:</label>
                <div>
                  <TextField
                    error={errorMessageflag}
                    helperText={errorMessageflag}
                    name="username"
                    autoFocus
                    placeholder="Username"
                    multiline
                    fullWidth
                    required
                    variant="outlined"
                    onChange={(e) => {
                      pristine = false;
                      setUsername(e.target.value);
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <PersonOutlineOutlinedIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                </div>
              </div>
              <div className="field">
                <label>Password:</label>
                <div>
                  <TextField
                    error={errorMessageflag}
                    helperText={errorMessageflag}
                    variant="outlined"
                    required
                    fullWidth
                    placeholder={"Password"}
                    name="password"
                    type={"password"}
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <LockOutlinedIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                </div>
              </div>

              <div className="field">
                <Field
                  name="rememberme"
                  component="input"
                  type="checkbox"
                ></Field>
                <div></div>
                <label>Remember me</label>
              </div>
              {loading ? (
                <img src={loadingGif} alt="wait gif"></img>
              ) : (
                <button
                  onClick={() => {
                    form.change("formScenario", "sign_in");
                  }}
                  disabled={username === "" || password === ""}
                  className="field"
                >
                  Login
                </button>
              )}
            </div>
          </form>
        )}
      </Form>
    </div>
  );
});
