"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.hangup = exports.call = exports.connectWebrtc = exports.initSession = exports.simpleUser = exports.domain = void 0;
var sip_js_1 = require("sip.js");
exports.domain = "68.183.220.137";
function getAudioElement(id) {
    var el = document.getElementById(id);
    if (!(el instanceof HTMLAudioElement)) {
        throw new Error("Element \"" + id + "\" not found or not an audio element.");
    }
    return el;
}
// Helper function to wait
var aor = "sip:alice@example.com";
function initSession() {
    return __awaiter(this, void 0, void 0, function () {
        var audio1, options, target, displayName, transportOptions, userAgent, inviterOptions, constraints, session;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getAudioElement("remoteAudio")];
                case 1:
                    audio1 = _a.sent();
                    options = {
                        aor: aor,
                        media: {
                            remote: {
                                audio: audio1
                            }
                        }
                    };
                    target = sip_js_1.UserAgent.makeURI("sip:bob@example.com");
                    if (!target) {
                        throw new Error("Failed to create target URI.");
                    }
                    displayName = "wss://webrtc01.phone.do:443";
                    transportOptions = {
                        server: "wss://webrtc01.phone.do:443"
                    };
                    userAgent = new sip_js_1.UserAgent({ transportOptions: transportOptions });
                    inviterOptions = {};
                    constraints = { audio: true, video: false };
                    if (!inviterOptions.sessionDescriptionHandlerOptions) {
                        inviterOptions.sessionDescriptionHandlerOptions = {};
                    }
                    if (!inviterOptions.sessionDescriptionHandlerOptions.constraints) {
                        inviterOptions.sessionDescriptionHandlerOptions.constraints = constraints;
                    }
                    session = new sip_js_1.Inviter(userAgent, target, inviterOptions);
                    return [2 /*return*/, session];
            }
        });
    });
}
exports.initSession = initSession;
// Main function
function connectWebrtc() {
    return __awaiter(this, void 0, Promise, function () {
        var audio1, server, options;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getAudioElement("remoteAudio")];
                case 1:
                    audio1 = _a.sent();
                    server = "wss://webrtc01.phone.do:443";
                    options = {
                        aor: aor,
                        media: {
                            remote: {
                                audio: audio1
                            }
                        }
                    };
                    // Construct a SimpleUser instance
                    exports.simpleUser = new sip_js_1.Web.SimpleUser(server, options);
                    // Supply delegate to handle inbound calls (optional)
                    exports.simpleUser.delegate = {
                        onCallReceived: function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, exports.simpleUser.answer()];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); }
                    };
                    return [2 /*return*/];
            }
        });
    });
}
exports.connectWebrtc = connectWebrtc;
function call(destination) {
    return __awaiter(this, void 0, Promise, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: 
                // Connect to server
                return [4 /*yield*/, exports.simpleUser.connect()];
                case 1:
                    // Connect to server
                    _a.sent();
                    // Register to receive inbound calls (optional)
                    return [4 /*yield*/, exports.simpleUser.register()];
                case 2:
                    // Register to receive inbound calls (optional)
                    _a.sent();
                    // Place call to the destination
                    return [4 /*yield*/, exports.simpleUser.call(destination)];
                case 3:
                    // Place call to the destination
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
exports.call = call;
function hangup() {
    return __awaiter(this, void 0, Promise, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, exports.simpleUser.hangup()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
exports.hangup = hangup;
