import axios, { AxiosInstance } from "axios";
import { isTokenExpired } from "./jwt";
import { Extension } from "../model/user-onboarding";
import { RecordFile, RecordingForm } from "../stores/recording-store";
import { HistoryForm } from "../stores/history-store";
import { Call } from "../stores/dialer-store";

export const USER_TOKEN_KEY = "user_token";

class ApiClient {
  private _client: AxiosInstance;
  constructor() {
    this._client = axios;
    axios.interceptors.response.use(
      (response) => response,
      (error) => {
        let status;
        try {
          status = error.response.status;
        }
        catch {
          return Promise.reject(error);

        }
        if (status === 500) {

        }
        if (status === 422) {
        }
        if (
          status === 401
        ) {
          // localStorage.removeItem(USER_TOKEN_KEY);
          console.log("error", error);
          // return "hello"

          return Promise.reject({ err: error.response.data, response: null })
        }
        return Promise.reject(error);
      }
    );
    this._client.defaults.baseURL = "http://localhost:3000/" || "";
  }

  private get client() {
    const token =localStorage.getItem(USER_TOKEN_KEY);
    this._client.defaults.headers["Content-Type"] = "application/ld+json";
    this._client.defaults.headers.Accept = "application/ld+json";
    this._client.defaults.headers.Authorization =
      token && !isTokenExpired(token) ? `Bearer ${token}` : undefined;

    return this._client;
  }

  login(username: string, password: string) {
    return this._client
      .post(
        "user/login",
        {
          username: username,
          password: password,
        },
        { headers: { "Content-type": "application/json" } }
      )
      .then((response) => response.data);
  }

  searchRecording(params: RecordingForm) {
    return this.client
      .get("recording/recordings/list", { params })
      .then((response) => response.data);
  }

  searchHistory(params: HistoryForm) {
    return this.client
      .get("cdrs/list", { params })
      .then((response) => response.data);
  }
  getRecordFile(params: RecordFile) {
    return this.client
      .get("recording/recordings/get", { params })
      .then((response) => response.data);
  }
  getRecordGroupsList() {
    return this.client.get("recording/groups/list").then((response) => {
      return response.data;
    });
  }

  getActiveCalls() {
    return this.client.get<Call[]>("calls/active/list").then((response) => {
      return response.data;
    });
  }
  userOnboardingFetch() {

    return this.client
      .post<Extension[]>(`phones/list`, {
        headers: { "Content-type": "application/json" },
      })
      .then((response) => response.data);
  }

}
export default new ApiClient();
